<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://www.pondybiz.com/
 * @since      1.0.0
 *
 * @package    Pondybiz
 * @subpackage Pondybiz/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Pondybiz
 * @subpackage Pondybiz/includes
 * @author     Pondybiz <info@pondybiz.com>
 */
class Pondybiz_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
