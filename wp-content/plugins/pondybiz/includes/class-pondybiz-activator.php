<?php

/**
 * Fired during plugin activation
 *
 * @link       http://www.pondybiz.com/
 * @since      1.0.0
 *
 * @package    Pondybiz
 * @subpackage Pondybiz/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Pondybiz
 * @subpackage Pondybiz/includes
 * @author     Pondybiz <info@pondybiz.com>
 */
class Pondybiz_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
