<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://www.pondybiz.com/
 * @since      1.0.0
 *
 * @package    Pondybiz
 * @subpackage Pondybiz/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Pondybiz
 * @subpackage Pondybiz/admin
 * @author     Pondybiz <info@pondybiz.com>
 */
class Pondybiz_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Pondybiz_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Pondybiz_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/pondybiz-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Pondybiz_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Pondybiz_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/pondybiz-admin.js', array( 'jquery' ), $this->version, false );

	}
	public function myplugin_social_options_page() {
        add_options_page('Page Title', 'Social Media', 'manage_options','social_media' ,array($this, 'myplugin_social_media_options_page'));
	}
	
	public function myplugin_social_media_options_page() {
		?>
		
		<h2>Social Media</h2>
		<form method="post" action="options.php">
		<?php settings_fields( 'myplugin_social_options_group' ); ?>
		<!-- Facebook -->
		<h3 style="text-decoration:underline">Facebook:</h3>
		<table>
			<tr valign="top">
			<th scope="row"><label for="new_fb_url">Facebook URL :</label></th>
			<td><input style="width:25em; margin-left:11.5em" type="text" id="new_fb_url" name="new_fb_url" value="<?php echo get_option('new_fb_url'); ?>" /></td>
			</tr>
		</table>
		<!-- Google+ -->
		<h3 style="text-decoration:underline">Google+:</h3>
		<table>
			<tr valign="top">
			<th scope="row"><label for="new_g+_url">Google+ URL :</label></th>
			<td><input style="width:25em; margin-left:11.8em" type="text" id="new_g+_url" name="new_g+_url" value="<?php echo get_option('new_g+_url'); ?>" /></td>
			</tr>
		</table>
		<!-- Instagram -->
		<h3 style="text-decoration:underline">Instagram:</h3>
			<table>
			<tr valign="top">
			<th scope="row"><label for="new_insta_url">Instagram URL :</label></th>
			<td><input style="width:25em; margin-left:11em" type="text" id="new_insta_url" name="new_insta_url" value="<?php echo get_option('new_insta_url'); ?>" /></td>
			</tr>
		</table>
		<?php  submit_button(); ?>
		</form>
		</div>
		<?php
	}

	function myplugin_register_options_page() {
		add_options_page('Page Title', 'Contact Details', 'manage_options', 'contact_det', array($this, 'myplugin_options_page'));
	}
	
	function myplugin_options_page() {
		?>
	  
		  <!-- Header &amp; Footer Location -->
		  <div>
		  <?php screen_icon(); ?>
		  <h2>Header &amp; Footer Location</h2>
		  <form method="post" action="options.php">
		  <?php settings_fields( 'myplugin_options_group' ); ?>
		  <!-- Location -->
		  <h3 style="text-decoration:underline">Location Section:</h3>
		  <table>
		  <tr valign="top">
		  <th scope="row"><label for="new_option_text">Location Text :</label></th>
		  <td><input style="width:25em; margin-left:12em" type="text" id="new_option_text" name="new_option_text" value="<?php echo get_option('new_option_text'); ?>" /></td>
		  </tr>
		  <tr valign="top">
		  <th scope="row"><label for="new_option_url">Location URL :</label></th>
		  <td><input style="width:25em; margin-left:12em" type="text" id="new_option_url" name="new_option_url" value="<?php echo get_option('new_option_url'); ?>" /></td>
		  </tr>
		  </table>
		  <!-- Email -->
		  <h3 style="text-decoration:underline">Email Section:</h3>
		  <table>
		  <tr valign="top">
		  <th scope="row"><label for="new_email_text">Email Text :</label></th>
		  <td><input style="width:25em; margin-left:13.3em" type="text" id="new_email_text" name="new_email_text" value="<?php echo get_option('new_email_text'); ?>" /></td>
		  </tr>
		  <tr valign="top">
		  <th scope="row"><label for="new_email_url">Email URL :</label></th>
		  <td><input style="width:25em; margin-left:13.3em" type="text" id="new_email_url" name="new_email_url" value="<?php echo get_option('new_email_url'); ?>" /></td>
		  </tr>
		  </table>
		  <!-- Call -->
		  <h3 style="text-decoration:underline">Call Section:</h3>
		  <table>
		  <tr valign="top">
		  <th scope="row"><label for="new_call_text">Call Text :</label></th>
		  <td><input style="width:25em; margin-left:14em" type="text" id="new_call_text" name="new_call_text" value="<?php echo get_option('new_call_text'); ?>" /></td>
		  </tr>
		  <tr valign="top">
		  <th scope="row"><label for="new_call_url">Call URL :</label></th>
		  <td><input style="width:25em; margin-left:14em" type="text" id="new_call_url" name="new_call_url" value="<?php echo get_option('new_call_url'); ?>" /></td>
		  </tr>
		  </table>
		  <?php  submit_button(); ?>
		  </form>
		  </div>
		<?php
	}

	public function productExtraField()
	{
		
		add_meta_box( 'product_meta_box', __( 'Prodcut extra field', $this->plugin_name), array($this,'product_build_meta_box'), 'post', 'normal', 'low' );
	}

	public function product_build_meta_box($post)
	{
		wp_nonce_field( 'product_meta_box', 'product_meta_box_nonce' );
		global $post;
		$short_title_value= get_post_meta( $post->ID, 'short_title', true );
		$extra_content_value= get_post_meta( $post->ID, 'extra_content', true );
		
		?>
		<table>
		<tr valign="top">
		  <th scope="row"><label for="short_title">Short Title</label></th>
		  <td>
		  <input style="width:25em; margin-left:14em" type="text" id="short_title" name="short_title" value="<?php echo $short_title_value?>" />
		  </td>
		  </tr>

		</table>

		<table>
		<tr valign="top">
		  <th scope="row"><label for="extra_content">extra</label></th>
		  <td>	<textarea name="extra_content" id="" cols="100" rows="10"><?php echo $extra_content_value?></textarea></td>
		  </tr>

		</table>
<?php }

public function product_save_meta_box_data( $post_id ) {

	/*
	 * We need to verify this came from our screen and with proper authorization,
	 * because the save_post action can be triggered at other times.
	 */
  
	
	if ( !isset( $_POST['product_meta_box_nonce'] ) ) {
			return;
	}
	
	// Verify that the nonce is valid.
	if ( !wp_verify_nonce( $_POST['product_meta_box_nonce'], 'product_meta_box' ) ) {
			return;
	}
	
	// If this is an autosave, our form has not been submitted, so we don't want to do anything.
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return;
	}

	// Check the user's permissions.
	if ( !current_user_can( 'edit_post', $post_id ) ) {
			return;
	}
	
//  var_dump($post_id);
// 	 die();
	// Sanitize user input.
	$short_title_meta_value = ( isset( $_POST['short_title'] ) ?  $_POST['short_title']  : '' );

	$extra_content = ( isset( $_POST['extra_content'] ) ?  $_POST['extra_content']  : '' );
	
	
	//var_dump($walkin_date_value);
	//var_dump($walkin_venue_value);
	//die();
	
	 //UPDATE
	 update_post_meta( $post_id, 'short_title',  $short_title_meta_value);
	 update_post_meta( $post_id, 'extra_content',  $extra_content);
	 


}


}